# Moogsoft SaaS

Vendor: Moogsoft
Homepage: https://www.moogsoft.com/

Product: Moogsoft SaaS 
Product Page: https://docs.moogsoft.com/moogsoft-cloud/en/index-en.html

## Introduction
We classify Moogsoft SaaS into the Service Assurance domain as Moogsoft SaaS monitors, detects, and responds to incidents. 

## Why Integrate
The Moogsoft SaaS adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Moogsoft SaaS. With this adapter you have the ability to perform operations on items such as:

- Alerts

## Additional Product Documentation
[APEX AIOps Incident Management APIs](https://docs.moogsoft.com/moogsoft-cloud/en/apis.html)

[Moogsoft Incident Management API Documentation](https://api.docs.moogsoft.com/docs/latest/branches/main/1sy0hr6odnj10-incident-management-api-documentation)