## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Moogsoft Saas. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Moogsoft Saas.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>


### Specific Adapter Calls

Specific adapter calls are built based on the API of the Moogsoft SaaS. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">alertDetails(alertId, callback)</td>
    <td style="padding:15px">alertDetails</td>
    <td style="padding:15px">{base_path}/{version}/v1/alerts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAlert(alertId, body, callback)</td>
    <td style="padding:15px">updateAlert</td>
    <td style="padding:15px">{base_path}/{version}/v1/alerts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAlerts(filter, incidentId, limit, sortBy, sortOrder = 'asc', start, utcOffset, callback)</td>
    <td style="padding:15px">listAlerts</td>
    <td style="padding:15px">{base_path}/{version}/v1/alerts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAlerts(body, callback)</td>
    <td style="padding:15px">updateAlerts</td>
    <td style="padding:15px">{base_path}/{version}/v1/alerts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">incidentIds(alertId, callback)</td>
    <td style="padding:15px">incidentIds</td>
    <td style="padding:15px">{base_path}/{version}/v1/alerts/{pathv1}/incidents?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">alertDtoColumnNames(callback)</td>
    <td style="padding:15px">alertDtoColumnNames</td>
    <td style="padding:15px">{base_path}/{version}/v1/alerts-columns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlertCount(filter, limit, callback)</td>
    <td style="padding:15px">getAlertCount</td>
    <td style="padding:15px">{base_path}/{version}/v1/alerts-count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">search(eventTime, namespace, callback)</td>
    <td style="padding:15px">search</td>
    <td style="padding:15px">{base_path}/{version}/v1/alerts-search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">alertSummaries(begin, buckets, end, callback)</td>
    <td style="padding:15px">alertSummaries</td>
    <td style="padding:15px">{base_path}/{version}/v1/alerts-summaries?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAutomationConfig(callback)</td>
    <td style="padding:15px">getAutomationConfig</td>
    <td style="padding:15px">{base_path}/{version}/v1/automation-config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchAutomationConfig(body, callback)</td>
    <td style="padding:15px">patchAutomationConfig</td>
    <td style="padding:15px">{base_path}/{version}/v1/automation-config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">testWorkflow(body, callback)</td>
    <td style="padding:15px">testWorkflow</td>
    <td style="padding:15px">{base_path}/{version}/v1/test-workflows?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getActions(callback)</td>
    <td style="padding:15px">getActions</td>
    <td style="padding:15px">{base_path}/{version}/v1/workflow-actions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDraftEndpointConfig(endpointId, callback)</td>
    <td style="padding:15px">getDraftEndpointConfig</td>
    <td style="padding:15px">{base_path}/{version}/v2/integrations/byoapi/{pathv1}/config/draft?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDraftEndpointConfig(endpointId, body, callback)</td>
    <td style="padding:15px">updateDraftEndpointConfig</td>
    <td style="padding:15px">{base_path}/{version}/v2/integrations/byoapi/{pathv1}/config/draft?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLiveEndpointConfig(endpointId, callback)</td>
    <td style="padding:15px">getLiveEndpointConfig</td>
    <td style="padding:15px">{base_path}/{version}/v2/integrations/byoapi/{pathv1}/config/live?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateLiveEndpointConfig(endpointId, body, callback)</td>
    <td style="padding:15px">updateLiveEndpointConfig</td>
    <td style="padding:15px">{base_path}/{version}/v2/integrations/byoapi/{pathv1}/config/live?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">testEndpointConfig(endpointId, body, callback)</td>
    <td style="padding:15px">testEndpointConfig</td>
    <td style="padding:15px">{base_path}/{version}/v2/integrations/byoapi/{pathv1}/config/test?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllEndpoints(callback)</td>
    <td style="padding:15px">getAllEndpoints</td>
    <td style="padding:15px">{base_path}/{version}/v1/integrations/byoapi?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCustomEndpoint(body, callback)</td>
    <td style="padding:15px">createCustomEndpoint</td>
    <td style="padding:15px">{base_path}/{version}/v1/integrations/byoapi?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCustomEndpoint(endpointId, callback)</td>
    <td style="padding:15px">getCustomEndpoint</td>
    <td style="padding:15px">{base_path}/{version}/v1/integrations/byoapi/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCustomEndpoint(endpointId, body, callback)</td>
    <td style="padding:15px">updateCustomEndpoint</td>
    <td style="padding:15px">{base_path}/{version}/v1/integrations/byoapi/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCustomEndpoint(endpointId, callback)</td>
    <td style="padding:15px">deleteCustomEndpoint</td>
    <td style="padding:15px">{base_path}/{version}/v1/integrations/byoapi/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postJsonPayload(endpointName, uuid, body, callback)</td>
    <td style="padding:15px">postJsonPayload</td>
    <td style="padding:15px">{base_path}/{version}/v1/integrations/custom/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRecentPayload(endpointId, id, callback)</td>
    <td style="padding:15px">getRecentPayload</td>
    <td style="padding:15px">{base_path}/{version}/v1/integrations/byoapi/{pathv1}/traffic/payloads/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTrafficSummary(endpointId, callback)</td>
    <td style="padding:15px">getTrafficSummary</td>
    <td style="padding:15px">{base_path}/{version}/v1/integrations/byoapi/{pathv1}/traffic/summary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllRecentPayloads(endpointId, maxResults, callback)</td>
    <td style="padding:15px">getAllRecentPayloads</td>
    <td style="padding:15px">{base_path}/{version}/v1/integrations/byoapi/{pathv1}/traffic/payloads?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">posttestEndpointConfig(endpointId, body, callback)</td>
    <td style="padding:15px">testEndpointConfig</td>
    <td style="padding:15px">{base_path}/{version}/v1/integrations/byoapi/{pathv1}/config/test?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getgetLiveEndpointConfig(endpointId, callback)</td>
    <td style="padding:15px">getLiveEndpointConfig</td>
    <td style="padding:15px">{base_path}/{version}/v1/integrations/byoapi/{pathv1}/config/live?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putupdateLiveEndpointConfig(endpointId, body, callback)</td>
    <td style="padding:15px">updateLiveEndpointConfig</td>
    <td style="padding:15px">{base_path}/{version}/v1/integrations/byoapi/{pathv1}/config/live?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getgetDraftEndpointConfig(endpointId, callback)</td>
    <td style="padding:15px">getDraftEndpointConfig</td>
    <td style="padding:15px">{base_path}/{version}/v1/integrations/byoapi/{pathv1}/config/draft?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putupdateDraftEndpointConfig(endpointId, body, callback)</td>
    <td style="padding:15px">updateDraftEndpointConfig</td>
    <td style="padding:15px">{base_path}/{version}/v1/integrations/byoapi/{pathv1}/config/draft?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">uploadNewCatalog(name, description, file, callback)</td>
    <td style="padding:15px">uploadNewCatalog</td>
    <td style="padding:15px">{base_path}/{version}/v1/catalog-files?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCatalogDocument(catalogName, documentId, callback)</td>
    <td style="padding:15px">deleteCatalogDocument</td>
    <td style="padding:15px">{base_path}/{version}/v1/catalogs/{pathv1}/documents/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyCatalogDocument(catalogName, documentId, body, callback)</td>
    <td style="padding:15px">modifyCatalogDocument</td>
    <td style="padding:15px">{base_path}/{version}/v1/catalogs/{pathv1}/documents/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCatalogs(callback)</td>
    <td style="padding:15px">getCatalogs</td>
    <td style="padding:15px">{base_path}/{version}/v1/catalogs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNewEmptyCatalog(body, callback)</td>
    <td style="padding:15px">createNewEmptyCatalog</td>
    <td style="padding:15px">{base_path}/{version}/v1/catalogs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addCatalogDocuments(catalogName, body, callback)</td>
    <td style="padding:15px">addCatalogDocuments</td>
    <td style="padding:15px">{base_path}/{version}/v1/catalogs/{pathv1}/batch?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCatalog(catalogName, callback)</td>
    <td style="padding:15px">getCatalog</td>
    <td style="padding:15px">{base_path}/{version}/v1/catalogs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCatalog(catalogName, callback)</td>
    <td style="padding:15px">deleteCatalog</td>
    <td style="padding:15px">{base_path}/{version}/v1/catalogs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">queryCatalogDocuments(catalogName, limit, offset, query, callback)</td>
    <td style="padding:15px">queryCatalogDocuments</td>
    <td style="padding:15px">{base_path}/{version}/v1/catalogs/{pathv1}/documents?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addCatalogDocument(catalogName, body, callback)</td>
    <td style="padding:15px">addCatalogDocument</td>
    <td style="padding:15px">{base_path}/{version}/v1/catalogs/{pathv1}/documents?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchCatalogDocuments(catalogName, limit, offset, body, callback)</td>
    <td style="padding:15px">searchCatalogDocuments</td>
    <td style="padding:15px">{base_path}/{version}/v1/catalogs/{pathv1}/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIntegrationByName(name, callback)</td>
    <td style="padding:15px">getIntegrationByName</td>
    <td style="padding:15px">{base_path}/{version}/v1/integrations/azure/appinsights/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIntegrationByName(name, callback)</td>
    <td style="padding:15px">deleteIntegrationByName</td>
    <td style="padding:15px">{base_path}/{version}/v1/integrations/azure/appinsights/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editIntegration(name, body, callback)</td>
    <td style="padding:15px">editIntegration</td>
    <td style="padding:15px">{base_path}/{version}/v1/integrations/azure/appinsights/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">testIntegrationCredentials(body, callback)</td>
    <td style="padding:15px">testIntegrationCredentials</td>
    <td style="padding:15px">{base_path}/{version}/v1/integrations/azure/appinsights/test?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listIntegrations(callback)</td>
    <td style="padding:15px">listIntegrations</td>
    <td style="padding:15px">{base_path}/{version}/v1/integrations/azure/appinsights?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNewIntegration(body, callback)</td>
    <td style="padding:15px">createNewIntegration</td>
    <td style="padding:15px">{base_path}/{version}/v1/integrations/azure/appinsights?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudwatchDetectors(name, callback)</td>
    <td style="padding:15px">getCloudwatchDetectors</td>
    <td style="padding:15px">{base_path}/{version}/v1/integrations/aws/{pathv1}/detectors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCloudwatchDetectors(name, body, callback)</td>
    <td style="padding:15px">updateCloudwatchDetectors</td>
    <td style="padding:15px">{base_path}/{version}/v1/integrations/aws/{pathv1}/detectors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudwatch(name, callback)</td>
    <td style="padding:15px">deleteCloudwatch</td>
    <td style="padding:15px">{base_path}/{version}/v1/integrations/aws/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCloudwatch(name, body, callback)</td>
    <td style="padding:15px">updateCloudwatch</td>
    <td style="padding:15px">{base_path}/{version}/v1/integrations/aws/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudwatchStatus(name, callback)</td>
    <td style="padding:15px">getCloudwatchStatus</td>
    <td style="padding:15px">{base_path}/{version}/v1/integrations/aws/{pathv1}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cloudwatchAccountId(callback)</td>
    <td style="padding:15px">cloudwatchAccountId</td>
    <td style="padding:15px">{base_path}/{version}/v1/integrations/aws/account?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">testCloudwatch(body, callback)</td>
    <td style="padding:15px">testCloudwatch</td>
    <td style="padding:15px">{base_path}/{version}/v1/integrations/aws/credentials/test?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listCloudwatch(callback)</td>
    <td style="padding:15px">listCloudwatch</td>
    <td style="padding:15px">{base_path}/{version}/v1/integrations/aws?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCloudwatch(body, callback)</td>
    <td style="padding:15px">createCloudwatch</td>
    <td style="padding:15px">{base_path}/{version}/v1/integrations/aws?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInstallCommand(platform = 'LINUX', callback)</td>
    <td style="padding:15px">getInstallCommand</td>
    <td style="padding:15px">{base_path}/{version}/v2/collector-installer/curl?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">download(arch = 'ARM64', platform = 'LINUX', version, callback)</td>
    <td style="padding:15px">download</td>
    <td style="padding:15px">{base_path}/{version}/v2/collector-installer/download?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">latestVersion(arch = 'ARM64', platform = 'LINUX', callback)</td>
    <td style="padding:15px">latestVersion</td>
    <td style="padding:15px">{base_path}/{version}/v2/collector-installer/latest-version?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInstallScript(platform = 'LINUX', callback)</td>
    <td style="padding:15px">getInstallScript</td>
    <td style="padding:15px">{base_path}/{version}/v2/collector-installer/script?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCollectors(callback)</td>
    <td style="padding:15px">getCollectors</td>
    <td style="padding:15px">{base_path}/{version}/v2/collectors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">registerCollector(body, callback)</td>
    <td style="padding:15px">registerCollector</td>
    <td style="padding:15px">{base_path}/{version}/v2/collectors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCollector(id, callback)</td>
    <td style="padding:15px">deleteCollector</td>
    <td style="padding:15px">{base_path}/{version}/v2/collectors/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">processHeartbeats(id, body, callback)</td>
    <td style="padding:15px">processHeartbeats</td>
    <td style="padding:15px">{base_path}/{version}/v2/collectors/{pathv1}/heartbeats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogs(id, callback)</td>
    <td style="padding:15px">getLogs</td>
    <td style="padding:15px">{base_path}/{version}/v2/collectors/{pathv1}/logs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">saveLogs(id, body, callback)</td>
    <td style="padding:15px">saveLogs</td>
    <td style="padding:15px">{base_path}/{version}/v2/collectors/{pathv1}/logs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAndClearMessages(id, callback)</td>
    <td style="padding:15px">getAndClearMessages</td>
    <td style="padding:15px">{base_path}/{version}/v2/collectors/{pathv1}/messages?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">generateTopology(id, callback)</td>
    <td style="padding:15px">generateTopology</td>
    <td style="padding:15px">{base_path}/{version}/v2/collectors/{pathv1}/topology?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">findPluginConfig(collectorId, pluginName, callback)</td>
    <td style="padding:15px">findPluginConfig</td>
    <td style="padding:15px">{base_path}/{version}/v2/plugin-config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPluginConfig(body, callback)</td>
    <td style="padding:15px">createPluginConfig</td>
    <td style="padding:15px">{base_path}/{version}/v2/plugin-config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteMatchingPluginConfig(collectorId, callback)</td>
    <td style="padding:15px">deleteMatchingPluginConfig</td>
    <td style="padding:15px">{base_path}/{version}/v2/plugin-config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPluginConfig(id, callback)</td>
    <td style="padding:15px">getPluginConfig</td>
    <td style="padding:15px">{base_path}/{version}/v2/plugin-config/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePluginConfig(id, callback)</td>
    <td style="padding:15px">deletePluginConfig</td>
    <td style="padding:15px">{base_path}/{version}/v2/plugin-config/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePluginConfig(id, body, callback)</td>
    <td style="padding:15px">updatePluginConfig</td>
    <td style="padding:15px">{base_path}/{version}/v2/plugin-config/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPlugins(collectorVersion, callback)</td>
    <td style="padding:15px">getPlugins</td>
    <td style="padding:15px">{base_path}/{version}/v2/plugins?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unixInstaller(callback)</td>
    <td style="padding:15px">unixInstaller</td>
    <td style="padding:15px">{base_path}/{version}/v1/collectors/install?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCollectorsDiscovery(uuid, body, callback)</td>
    <td style="padding:15px">postCollectorsDiscovery</td>
    <td style="padding:15px">{base_path}/{version}/v1/collectors/{pathv1}/discovery?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCurlCommand(collectOnStartup, instance, platform = 'LINUX', callback)</td>
    <td style="padding:15px">getCurlCommand</td>
    <td style="padding:15px">{base_path}/{version}/v1/collectors/curl?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">windowsInstaller(callback)</td>
    <td style="padding:15px">windowsInstaller</td>
    <td style="padding:15px">{base_path}/{version}/v1/collectors/install/windows?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">action(uuid, body, callback)</td>
    <td style="padding:15px">action</td>
    <td style="padding:15px">{base_path}/{version}/v1/collectors/{pathv1}/actions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">restartCollector(uuid, callback)</td>
    <td style="padding:15px">restartCollector</td>
    <td style="padding:15px">{base_path}/{version}/v1/collectors/{pathv1}/restart?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCollectorMetrics(uuid, fullyQualifiedMoob, metric, version, callback)</td>
    <td style="padding:15px">getCollectorMetrics</td>
    <td style="padding:15px">{base_path}/{version}/v1/collectors/{pathv1}/metrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCollectorMetrics(uuid, body, callback)</td>
    <td style="padding:15px">postCollectorMetrics</td>
    <td style="padding:15px">{base_path}/{version}/v1/collectors/{pathv1}/metrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDatums(body, callback)</td>
    <td style="padding:15px">getDatums</td>
    <td style="padding:15px">{base_path}/{version}/v1/collectors/datums?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">oldMetricsEndpoint(callback)</td>
    <td style="padding:15px">oldMetricsEndpoint</td>
    <td style="padding:15px">{base_path}/{version}/v1/collectors/metrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLatestVersion(arch = 'ARMV7L', platform = 'LINUX', callback)</td>
    <td style="padding:15px">getLatestVersion</td>
    <td style="padding:15px">{base_path}/{version}/v1/collectors/latest/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">fetchTar(arch = 'ARMV7L', platform = 'LINUX', callback)</td>
    <td style="padding:15px">fetchTar</td>
    <td style="padding:15px">{base_path}/{version}/v1/collectors/tar/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getItemConfig(attributes, component, encrypt, includeParent, owner, tenant, callback)</td>
    <td style="padding:15px">getItemConfig</td>
    <td style="padding:15px">{base_path}/{version}/v1/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setConfig(body, callback)</td>
    <td style="padding:15px">setConfig</td>
    <td style="padding:15px">{base_path}/{version}/v1/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfig(attributes, component, encrypt, fuzzy, owner, tenant, callback)</td>
    <td style="padding:15px">deleteConfig</td>
    <td style="padding:15px">{base_path}/{version}/v1/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfig(body, callback)</td>
    <td style="padding:15px">patchConfig</td>
    <td style="padding:15px">{base_path}/{version}/v1/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">bulkDeleteConfig(body, callback)</td>
    <td style="padding:15px">bulkDeleteConfig</td>
    <td style="padding:15px">{base_path}/{version}/v1/config/batch?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchConfig(attributes, component, encrypt, fuzzy, owner, tenant, callback)</td>
    <td style="padding:15px">searchConfig</td>
    <td style="padding:15px">{base_path}/{version}/v1/config/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCorrDefWithName(definitionName, callback)</td>
    <td style="padding:15px">deleteCorrDefWithName</td>
    <td style="padding:15px">{base_path}/{version}/v1/correlation-definitions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchCorrDefWithName(definitionName, body, callback)</td>
    <td style="padding:15px">patchCorrDefWithName</td>
    <td style="padding:15px">{base_path}/{version}/v1/correlation-definitions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCorrDef(uuid, callback)</td>
    <td style="padding:15px">deleteCorrDef</td>
    <td style="padding:15px">{base_path}/{version}/v1/correlation-definitions/id/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchCorrDef(uuid, body, callback)</td>
    <td style="padding:15px">patchCorrDef</td>
    <td style="padding:15px">{base_path}/{version}/v1/correlation-definitions/id/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCorrDef(callback)</td>
    <td style="padding:15px">getCorrDef</td>
    <td style="padding:15px">{base_path}/{version}/v1/correlation-definitions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCorrDef(body, callback)</td>
    <td style="padding:15px">createCorrDef</td>
    <td style="padding:15px">{base_path}/{version}/v1/correlation-definitions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">get(callback)</td>
    <td style="padding:15px">get</td>
    <td style="padding:15px">{base_path}/{version}/v1/tracking-data?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDatadogIntegration(name, callback)</td>
    <td style="padding:15px">getDatadogIntegration</td>
    <td style="padding:15px">{base_path}/{version}/v1/integrations/datadog/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDatadogIntegration(name, callback)</td>
    <td style="padding:15px">deleteDatadogIntegration</td>
    <td style="padding:15px">{base_path}/{version}/v1/integrations/datadog/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDatadogIntegration(name, body, callback)</td>
    <td style="padding:15px">updateDatadogIntegration</td>
    <td style="padding:15px">{base_path}/{version}/v1/integrations/datadog/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">testDatadogCredentials(body, callback)</td>
    <td style="padding:15px">testDatadogCredentials</td>
    <td style="padding:15px">{base_path}/{version}/v1/integrations/datadog/credentials/test?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllDatadogIntegrations(callback)</td>
    <td style="padding:15px">getAllDatadogIntegrations</td>
    <td style="padding:15px">{base_path}/{version}/v1/integrations/datadog?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDatadogIntegration(body, callback)</td>
    <td style="padding:15px">postDatadogIntegration</td>
    <td style="padding:15px">{base_path}/{version}/v1/integrations/datadog?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getColumns(callback)</td>
    <td style="padding:15px">getColumns</td>
    <td style="padding:15px">{base_path}/{version}/v1/events-columns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postEvents(body, callback)</td>
    <td style="padding:15px">postEvents</td>
    <td style="padding:15px">{base_path}/{version}/v1/integrations/events?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reindex(body, callback)</td>
    <td style="padding:15px">reindex</td>
    <td style="padding:15px">{base_path}/{version}/v1/index/reindex?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReindexStatus(callback)</td>
    <td style="padding:15px">getReindexStatus</td>
    <td style="padding:15px">{base_path}/{version}/v1/index/reindex-status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">incidentDtoColumnNames(callback)</td>
    <td style="padding:15px">incidentDtoColumnNames</td>
    <td style="padding:15px">{base_path}/{version}/v1/incidents-columns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">incidentsCount(filter, utcOffset, callback)</td>
    <td style="padding:15px">incidentsCount</td>
    <td style="padding:15px">{base_path}/{version}/v1/incidents/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listIncidents(filter, limit, sortBy, sortOrder, start, utcOffset, callback)</td>
    <td style="padding:15px">listIncidents</td>
    <td style="padding:15px">{base_path}/{version}/v1/incidents?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIncidents(body, callback)</td>
    <td style="padding:15px">updateIncidents</td>
    <td style="padding:15px">{base_path}/{version}/v1/incidents?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIncident(incidentId, body, callback)</td>
    <td style="padding:15px">updateIncident</td>
    <td style="padding:15px">{base_path}/{version}/v1/incidents/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIncidentSummaries(begin, buckets, end, callback)</td>
    <td style="padding:15px">getIncidentSummaries</td>
    <td style="padding:15px">{base_path}/{version}/v1/incidents-summaries?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIncidentTagsConfig(callback)</td>
    <td style="padding:15px">getIncidentTagsConfig</td>
    <td style="padding:15px">{base_path}/{version}/v1/incident-tags-config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchIncidentTagsConfig(body, callback)</td>
    <td style="padding:15px">patchIncidentTagsConfig</td>
    <td style="padding:15px">{base_path}/{version}/v1/incident-tags-config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIntegrationLogs(filterNames, filterValues, keyword, callback)</td>
    <td style="padding:15px">getIntegrationLogs</td>
    <td style="padding:15px">{base_path}/{version}/v1/integration-logs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIntegrationLogs(body, callback)</td>
    <td style="padding:15px">postIntegrationLogs</td>
    <td style="padding:15px">{base_path}/{version}/v1/integration-logs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">downloadMarFile(file, mar, namespace, callback)</td>
    <td style="padding:15px">downloadMarFile</td>
    <td style="padding:15px">{base_path}/{version}/v1/mars/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAvailableMars(callback)</td>
    <td style="padding:15px">getAvailableMars</td>
    <td style="padding:15px">{base_path}/{version}/v1/mars?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getgetMarFileChecksum(file, mar, namespace, callback)</td>
    <td style="padding:15px">getMarFileChecksum</td>
    <td style="padding:15px">{base_path}/{version}/v1/mars/{pathv1}/{pathv2}/{pathv3}/checksum?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMarConfig(theNameOfTheConfig, component, callback)</td>
    <td style="padding:15px">getMarConfig</td>
    <td style="padding:15px">{base_path}/{version}/v1/mars-config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postMarConfig(body, callback)</td>
    <td style="padding:15px">postMarConfig</td>
    <td style="padding:15px">{base_path}/{version}/v1/mars-config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">associateMarConfig(uuid, body, callback)</td>
    <td style="padding:15px">associateMarConfig</td>
    <td style="padding:15px">{base_path}/{version}/v1/mars-config/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMarCollectorConfig(mar, namespace, uuid, callback)</td>
    <td style="padding:15px">getMarCollectorConfig</td>
    <td style="padding:15px">{base_path}/{version}/v1/mars-config/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disassociateMarConfig1(mar, namespace, uuid, callback)</td>
    <td style="padding:15px">disassociateMarConfig1</td>
    <td style="padding:15px">{base_path}/{version}/v1/mars-config/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllDefinitions(callback)</td>
    <td style="padding:15px">getAllDefinitions</td>
    <td style="padding:15px">{base_path}/{version}/v1/collectors/mars/definitions/all?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disassociateMarConfig(mar, namespace, uuid, callback)</td>
    <td style="padding:15px">disassociateMarConfig</td>
    <td style="padding:15px">{base_path}/{version}/v1/collectors/mars/{pathv1}/config/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMarCollectorCredentials(mar, namespace, uuid, callback)</td>
    <td style="padding:15px">getMarCollectorCredentials</td>
    <td style="padding:15px">{base_path}/{version}/v1/collectors/mars/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disassociateMarCredentials(mar, namespace, uuid, callback)</td>
    <td style="padding:15px">disassociateMarCredentials</td>
    <td style="padding:15px">{base_path}/{version}/v1/collectors/mars/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">associateMarCredentials(uuid, body, callback)</td>
    <td style="padding:15px">associateMarCredentials</td>
    <td style="padding:15px">{base_path}/{version}/v1/collectors/mars/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMarCredentials(component, name, callback)</td>
    <td style="padding:15px">getMarCredentials</td>
    <td style="padding:15px">{base_path}/{version}/v1/collectors/mars/credentials?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDefinition(mar, namespace, callback)</td>
    <td style="padding:15px">getDefinition</td>
    <td style="padding:15px">{base_path}/{version}/v1/collectors/mars/definitions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getdownloadMarFile(file, callback)</td>
    <td style="padding:15px">downloadMarFile</td>
    <td style="padding:15px">{base_path}/{version}/v1/collectors/mars/file?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMarCollectorCredentialsRedirect(uuid, mar, namespace, callback)</td>
    <td style="padding:15px">getMarCollectorCredentialsRedirect</td>
    <td style="padding:15px">{base_path}/{version}/v1/collectors/mars/{pathv1}/credentials?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">associateMarCredentials2(uuid, body, callback)</td>
    <td style="padding:15px">associateMarCredentials2</td>
    <td style="padding:15px">{base_path}/{version}/v1/collectors/mars/{pathv1}/credentials?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMarFileChecksum(file, callback)</td>
    <td style="padding:15px">getMarFileChecksum</td>
    <td style="padding:15px">{base_path}/{version}/v1/collectors/mars/checksum?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMarCollectorConfigRedirect(uuid, mar, namespace, callback)</td>
    <td style="padding:15px">getMarCollectorConfigRedirect</td>
    <td style="padding:15px">{base_path}/{version}/v1/collectors/mars/{pathv1}/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">associateMarConfig1(uuid, body, callback)</td>
    <td style="padding:15px">associateMarConfig1</td>
    <td style="padding:15px">{base_path}/{version}/v1/collectors/mars/{pathv1}/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disassociateMarCredentials2(mar, namespace, uuid, callback)</td>
    <td style="padding:15px">disassociateMarCredentials2</td>
    <td style="padding:15px">{base_path}/{version}/v1/collectors/mars/{pathv1}/credentials/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMoobConfig1(uuid, fullyQualifiedMoob, callback)</td>
    <td style="padding:15px">getMoobConfig1</td>
    <td style="padding:15px">{base_path}/{version}/v1/collectors/{pathv1}/moobs/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postMoobConfig(uuid, body, callback)</td>
    <td style="padding:15px">postMoobConfig</td>
    <td style="padding:15px">{base_path}/{version}/v1/collectors/{pathv1}/moobs/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getgetAvailableMars(theNameOfTheConfig, component, name, callback)</td>
    <td style="padding:15px">getAvailableMars</td>
    <td style="padding:15px">{base_path}/{version}/v1/collectors/mars?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postMarConfig1(body, callback)</td>
    <td style="padding:15px">postMarConfig1</td>
    <td style="padding:15px">{base_path}/{version}/v1/collectors/mars?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getgetMarConfig(component, name, callback)</td>
    <td style="padding:15px">getMarConfig</td>
    <td style="padding:15px">{base_path}/{version}/v1/collectors/mars/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postMarConfig2(body, callback)</td>
    <td style="padding:15px">postMarConfig2</td>
    <td style="padding:15px">{base_path}/{version}/v1/collectors/mars/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMoob1(fullyQualifiedMoob, callback)</td>
    <td style="padding:15px">getMoob1</td>
    <td style="padding:15px">{base_path}/{version}/v1/collectors/mars/moobs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMoobConfig(uuid, fullyQualifiedMoob, callback)</td>
    <td style="padding:15px">getMoobConfig</td>
    <td style="padding:15px">{base_path}/{version}/v1/moobs/{pathv1}/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createMoobConfig(uuid, body, callback)</td>
    <td style="padding:15px">createMoobConfig</td>
    <td style="padding:15px">{base_path}/{version}/v1/moobs/{pathv1}/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMoob(fullyQualifiedMoob, callback)</td>
    <td style="padding:15px">getMoob</td>
    <td style="padding:15px">{base_path}/{version}/v1/moobs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">associateMarCredentials1(uuid, body, callback)</td>
    <td style="padding:15px">associateMarCredentials1</td>
    <td style="padding:15px">{base_path}/{version}/v1/mars-credentials/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMarCollectorCredentials1(mar, namespace, uuid, callback)</td>
    <td style="padding:15px">getMarCollectorCredentials1</td>
    <td style="padding:15px">{base_path}/{version}/v1/mars-credentials/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disassociateMarCredentials1(mar, namespace, uuid, callback)</td>
    <td style="padding:15px">disassociateMarCredentials1</td>
    <td style="padding:15px">{base_path}/{version}/v1/mars-credentials/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMarCredentials1(component, name, callback)</td>
    <td style="padding:15px">getMarCredentials1</td>
    <td style="padding:15px">{base_path}/{version}/v1/mars-credentials?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllDefinitions1(callback)</td>
    <td style="padding:15px">getAllDefinitions1</td>
    <td style="padding:15px">{base_path}/{version}/v1/mars-definitions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDefinition1(mar, namespace, callback)</td>
    <td style="padding:15px">getDefinition1</td>
    <td style="padding:15px">{base_path}/{version}/v1/mars-definitions/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllMetrics(callback)</td>
    <td style="padding:15px">getAllMetrics</td>
    <td style="padding:15px">{base_path}/{version}/v1/integrations/metrics/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateConfig(body, callback)</td>
    <td style="padding:15px">updateConfig</td>
    <td style="padding:15px">{base_path}/{version}/v1/integrations/metrics/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postMetric(body, callback)</td>
    <td style="padding:15px">postMetric</td>
    <td style="padding:15px">{base_path}/{version}/v1/integrations/metrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listPagerDutyIntegration(callback)</td>
    <td style="padding:15px">listPagerDutyIntegration</td>
    <td style="padding:15px">{base_path}/{version}/v1/integrations/pagerduty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPagerDutyIntegration(body, callback)</td>
    <td style="padding:15px">postPagerDutyIntegration</td>
    <td style="padding:15px">{base_path}/{version}/v1/integrations/pagerduty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPagerDutyIntegrationById(id, callback)</td>
    <td style="padding:15px">getPagerDutyIntegrationById</td>
    <td style="padding:15px">{base_path}/{version}/v1/integrations/pagerduty/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePagerDutyIntegration(id, callback)</td>
    <td style="padding:15px">deletePagerDutyIntegration</td>
    <td style="padding:15px">{base_path}/{version}/v1/integrations/pagerduty/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePagerDutyIntegation(id, body, callback)</td>
    <td style="padding:15px">updatePagerDutyIntegation</td>
    <td style="padding:15px">{base_path}/{version}/v1/integrations/pagerduty/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">testPagerDutyIntegration(body, callback)</td>
    <td style="padding:15px">testPagerDutyIntegration</td>
    <td style="padding:15px">{base_path}/{version}/v2/integrations/pagerduty-test?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">hello(body, callback)</td>
    <td style="padding:15px">hello</td>
    <td style="padding:15px">{base_path}/{version}/v1/quarkus/http/hello?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRollups(endTime, fullyQualifiedMoob, granularity = 'DAY', key, limit, metric, source, startTime, callback)</td>
    <td style="padding:15px">getRollups</td>
    <td style="padding:15px">{base_path}/{version}/v1/rollups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSuggestions(limit, name, value, callback)</td>
    <td style="padding:15px">getSuggestions</td>
    <td style="padding:15px">{base_path}/{version}/v1/datum-index-suggestions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDetailedDatums(endTime, fullyQualifiedMoob, key, limit, metric, source, startTime, uuid, callback)</td>
    <td style="padding:15px">getDetailedDatums</td>
    <td style="padding:15px">{base_path}/{version}/v1/datums/details?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDatumsDataPoints(fullyQualifiedMoob, key, limit, metric, source, callback)</td>
    <td style="padding:15px">getDatumsDataPoints</td>
    <td style="padding:15px">{base_path}/{version}/v1/datums-data-points?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDatumIndexes(direction = 'ASC', filter, limit, sortBy = 'DATUMS', start, callback)</td>
    <td style="padding:15px">getDatumIndexes</td>
    <td style="padding:15px">{base_path}/{version}/v1/datum-indexes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetDatumIndexes(body, callback)</td>
    <td style="padding:15px">postGetDatumIndexes</td>
    <td style="padding:15px">{base_path}/{version}/v1/datum-indexes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRollupStrategy(fullyQualifiedMoob, metric, callback)</td>
    <td style="padding:15px">getRollupStrategy</td>
    <td style="padding:15px">{base_path}/{version}/v1/rollup-strategy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setRollupStrategy(body, callback)</td>
    <td style="padding:15px">setRollupStrategy</td>
    <td style="padding:15px">{base_path}/{version}/v1/rollup-strategy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllApiKeys(callback)</td>
    <td style="padding:15px">getAllApiKeys</td>
    <td style="padding:15px">{base_path}/{version}/v1/keys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteMultipleApiKeys(id, callback)</td>
    <td style="padding:15px">deleteMultipleApiKeys</td>
    <td style="padding:15px">{base_path}/{version}/v1/keys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUserApiKeys(userId, callback)</td>
    <td style="padding:15px">getUserApiKeys</td>
    <td style="padding:15px">{base_path}/{version}/v2/users/{pathv1}/keys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createApiKey(userId, body, callback)</td>
    <td style="padding:15px">createApiKey</td>
    <td style="padding:15px">{base_path}/{version}/v2/users/{pathv1}/keys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApiKey(keyId, userId, callback)</td>
    <td style="padding:15px">getApiKey</td>
    <td style="padding:15px">{base_path}/{version}/v2/users/{pathv1}/keys/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteApiKey(keyId, userId, callback)</td>
    <td style="padding:15px">deleteApiKey</td>
    <td style="padding:15px">{base_path}/{version}/v2/users/{pathv1}/keys/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRoles(callback)</td>
    <td style="padding:15px">getRoles</td>
    <td style="padding:15px">{base_path}/{version}/v1/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsers(showDeleted, callback)</td>
    <td style="padding:15px">getUsers</td>
    <td style="padding:15px">{base_path}/{version}/v2/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createUser(body, callback)</td>
    <td style="padding:15px">createUser</td>
    <td style="padding:15px">{base_path}/{version}/v2/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUserDetails(id, callback)</td>
    <td style="padding:15px">getUserDetails</td>
    <td style="padding:15px">{base_path}/{version}/v2/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUser(id, callback)</td>
    <td style="padding:15px">deleteUser</td>
    <td style="padding:15px">{base_path}/{version}/v2/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editUser(id, body, callback)</td>
    <td style="padding:15px">editUser</td>
    <td style="padding:15px">{base_path}/{version}/v2/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebhook(id, callback)</td>
    <td style="padding:15px">getWebhook</td>
    <td style="padding:15px">{base_path}/{version}/v1/integrations/webhooks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteWebhook(id, callback)</td>
    <td style="padding:15px">deleteWebhook</td>
    <td style="padding:15px">{base_path}/{version}/v1/integrations/webhooks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyWebhook(id, body, callback)</td>
    <td style="padding:15px">modifyWebhook</td>
    <td style="padding:15px">{base_path}/{version}/v1/integrations/webhooks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">testWebhooksConfig(body, callback)</td>
    <td style="padding:15px">testWebhooksConfig</td>
    <td style="padding:15px">{base_path}/{version}/v1/integrations/webhook-test?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebhooksConfig(callback)</td>
    <td style="padding:15px">getWebhooksConfig</td>
    <td style="padding:15px">{base_path}/{version}/v1/integrations/webhooks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postWebhookConfig(body, callback)</td>
    <td style="padding:15px">postWebhookConfig</td>
    <td style="padding:15px">{base_path}/{version}/v1/integrations/webhooks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getgetWebhooksConfig(name, callback)</td>
    <td style="padding:15px">getWebhooksConfig</td>
    <td style="padding:15px">{base_path}/{version}/v2/integrations/webhooks/items?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postpostWebhookConfig(body, callback)</td>
    <td style="padding:15px">postWebhookConfig</td>
    <td style="padding:15px">{base_path}/{version}/v2/integrations/webhooks/items?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getgetWebhook(id, callback)</td>
    <td style="padding:15px">getWebhook</td>
    <td style="padding:15px">{base_path}/{version}/v2/integrations/webhooks/items/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletedeleteWebhook(id, callback)</td>
    <td style="padding:15px">deleteWebhook</td>
    <td style="padding:15px">{base_path}/{version}/v2/integrations/webhooks/items/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchmodifyWebhook(id, body, callback)</td>
    <td style="padding:15px">modifyWebhook</td>
    <td style="padding:15px">{base_path}/{version}/v2/integrations/webhooks/items/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">previewWebhook(body, callback)</td>
    <td style="padding:15px">previewWebhook</td>
    <td style="padding:15px">{base_path}/{version}/v2/integrations/webhooks/preview?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">posttestWebhooksConfig(body, callback)</td>
    <td style="padding:15px">testWebhooksConfig</td>
    <td style="padding:15px">{base_path}/{version}/v2/integrations/webhooks/test?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTemplatedActions(callback)</td>
    <td style="padding:15px">getTemplatedActions</td>
    <td style="padding:15px">{base_path}/{version}/v1/workflow-template-actions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createTemplateAction(body, callback)</td>
    <td style="padding:15px">createTemplateAction</td>
    <td style="padding:15px">{base_path}/{version}/v1/workflow-template-actions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTemplateAction(id, callback)</td>
    <td style="padding:15px">deleteTemplateAction</td>
    <td style="padding:15px">{base_path}/{version}/v1/workflow-template-actions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyTemplateAction(id, body, callback)</td>
    <td style="padding:15px">modifyTemplateAction</td>
    <td style="padding:15px">{base_path}/{version}/v1/workflow-template-actions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWorkflowMetricsExceptions(id, callback)</td>
    <td style="padding:15px">getWorkflowMetricsExceptions</td>
    <td style="padding:15px">{base_path}/{version}/v1/workflows/{pathv1}/metrics/exceptions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteWorkflowMetricsExceptions(id, callback)</td>
    <td style="padding:15px">deleteWorkflowMetricsExceptions</td>
    <td style="padding:15px">{base_path}/{version}/v1/workflows/{pathv1}/metrics/exceptions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWorkflowTest(id, callback)</td>
    <td style="padding:15px">getWorkflowTest</td>
    <td style="padding:15px">{base_path}/{version}/v1/workflows/{pathv1}/test?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyWorkflows(body, callback)</td>
    <td style="padding:15px">modifyWorkflows</td>
    <td style="padding:15px">{base_path}/{version}/v1/workflows-bulk?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWorkflow(id, callback)</td>
    <td style="padding:15px">getWorkflow</td>
    <td style="padding:15px">{base_path}/{version}/v1/workflows/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteWorkflow(id, callback)</td>
    <td style="padding:15px">deleteWorkflow</td>
    <td style="padding:15px">{base_path}/{version}/v1/workflows/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyWorkflow(id, body, callback)</td>
    <td style="padding:15px">modifyWorkflow</td>
    <td style="padding:15px">{base_path}/{version}/v1/workflows/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWorkflowStatus(id, callback)</td>
    <td style="padding:15px">getWorkflowStatus</td>
    <td style="padding:15px">{base_path}/{version}/v1/workflows/{pathv1}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setWorkflowStatus(id, body, callback)</td>
    <td style="padding:15px">setWorkflowStatus</td>
    <td style="padding:15px">{base_path}/{version}/v1/workflows/{pathv1}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setWorkflowPriorities(body, callback)</td>
    <td style="padding:15px">setWorkflowPriorities</td>
    <td style="padding:15px">{base_path}/{version}/v1/workflows-bulk/priority?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWorkflowMetrics(id, callback)</td>
    <td style="padding:15px">getWorkflowMetrics</td>
    <td style="padding:15px">{base_path}/{version}/v1/workflows/{pathv1}/metrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWorkflows(status = 'DELETED', callback)</td>
    <td style="padding:15px">getWorkflows</td>
    <td style="padding:15px">{base_path}/{version}/v1/workflows?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createWorkflow(body, callback)</td>
    <td style="padding:15px">createWorkflow</td>
    <td style="padding:15px">{base_path}/{version}/v1/workflows?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
