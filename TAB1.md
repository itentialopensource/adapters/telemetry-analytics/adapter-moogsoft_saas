# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Moogsoft_saas System. The API that was used to build the adapter for Moogsoft_saas is usually available in the report directory of this adapter. The adapter utilizes the Moogsoft_saas API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Moogsoft SaaS adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Moogsoft SaaS. With this adapter you have the ability to perform operations on items such as:

- Alerts

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
