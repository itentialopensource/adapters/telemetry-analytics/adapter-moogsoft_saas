
## 0.5.4 [10-15-2024]

* Changes made at 2024.10.14_20:28PM

See merge request itentialopensource/adapters/adapter-moogsoft_saas!13

---

## 0.5.3 [09-05-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-moogsoft_saas!11

---

## 0.5.2 [08-15-2024]

* Changes made at 2024.08.14_18:40PM

See merge request itentialopensource/adapters/adapter-moogsoft_saas!10

---

## 0.5.1 [08-07-2024]

* Changes made at 2024.08.06_19:52PM

See merge request itentialopensource/adapters/adapter-moogsoft_saas!9

---

## 0.5.0 [07-03-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/telemetry-analytics/adapter-moogsoft_saas!8

---

## 0.4.4 [03-27-2024]

* Changes made at 2024.03.27_13:16PM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-moogsoft_saas!7

---

## 0.4.3 [03-12-2024]

* Changes made at 2024.03.12_10:59AM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-moogsoft_saas!6

---

## 0.4.2 [02-27-2024]

* Changes made at 2024.02.27_11:34AM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-moogsoft_saas!5

---

## 0.4.1 [01-09-2024]

* fix link in meta

See merge request itentialopensource/adapters/telemetry-analytics/adapter-moogsoft_saas!4

---

## 0.4.0 [01-05-2024]

* Minor/2023 auto migration

See merge request itentialopensource/adapters/telemetry-analytics/adapter-moogsoft_saas!3

---

## 0.3.0 [01-03-2024]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/telemetry-analytics/adapter-moogsoft_saas!2

---

## 0.2.0 [05-27-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/telemetry-analytics/adapter-moogsoft_saas!1

---

## 0.1.2 [07-19-2021] & 0.1.1 [07-18-2021]

- Initial Commit

See commit 52b26c6

---
